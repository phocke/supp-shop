Create database.yml file
-

Copy & paste database.yml.template and fill with correct info

Create db if you need

    bundle exec rake db:create

Prepare your db

    bundle exec rake db:migrate && \
    bundle exec rake db:seed

Install bower (and node if you need to)

    npm install --global bower

Install JS dependencies with bower (from your project dir)

    bower install


Run the application

    foreman start

All development related tasks are handled by *foreman*.

* **thin** as web server
* **guard** for LiveReload

