paypal = { :preferred_server => "sandbox", :preferred_test_mode => true }
puts
puts "PayPal config"

print "  username: "
paypal[:preferred_login] = ::STDIN.gets.chomp
print "  password: "
paypal[:preferred_password] = ::STDIN.gets.chomp
print "  signature: "
paypal[:preferred_signature] = ::STDIN.gets.chomp

Spree::Gateway::PayPalExpress.create!({
    :name => "PayPal",
    :description => "PayPal"
  }.merge(paypal))