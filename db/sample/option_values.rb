size = Spree::OptionType.find_by_presentation!("Size")

[
  {
    :name => "100g",
    :presentation => "100g",
    :position => 1,
    :option_type => size
  },
  {
    :name => "250g",
    :presentation => "250g",
    :position => 2,
  },
  {
    :name => "500g",
    :presentation => "500g",
    :position => 3,
  },
  {
    :name => "1kg",
    :presentation => "1kg",
    :position => 4,
  }
].each do |attrs|
  option = Spree::OptionValue.new
  option.name = attrs[:name]
  option.presentation = attrs[:presentation]
  option.position = attrs[:position]
  option.option_type = size

  option.save!
end