# encoding: utf-8

food = Spree::TaxCategory.find_by_name!("Food")
shipping_category = Spree::ShippingCategory.find_by_name!("Default")

default_attrs = {
  :description => Faker::Lorem.paragraph,
  :available_on => Time.zone.now
}

default_shipping_category = Spree::ShippingCategory.find_by_name!("Default")

products = [
  {
    name: "Optimum Nutrition BCAA 1000 Caps",
    description: "BCAA 1000 contains a potent balance of Branched Chain Amino Acids which are building blocks of muscle mass and size. Metabolized directly in the muscle, BCAAs, may improve nitrogen retention by sparing other amino acid groups for repair and rebuilding. The Branched Chain Amino Acids (BCAA's), so named because of their unique branched chemical structure, are essential in that the body cannot make them from other compounds. In recent times, these amino acids have been investigated for their potential anti-catabolic (preventing muscle breakdown) and anabolic (muscle building) effects." ,
    price: 240,
    photo: "http://images.iherb.com/l/OPN-02036-2.jpg",
    form: "Caps",
    category: "BCAA & Amino Acids",
    brand: "Optimum Nutrition",
    goal: "Loose weight",
  },
  {
    name: "Gaspari Glutamine 300g",
    description: "Glutamine from Gaspari Nutrition has set a new standard for quality and purity amongst L-Glutamine supplements!  Glutamine uses only pure, micronized, pharmaceutical grade L-Glutamine to ensure the highest quality and that it mixes into your chosen beverage and doesn’t collect on the bottom of the cup before you finish drinking it. L-Glutamine is a very slightly sweet, odourless, crystalline and pure powdered amino acid.L-Glutamine is perhaps the most revered immune-boosting, muscle-hydrating and muscle-recovery dietary supplement in the sports nutrition industry. Most pro and other elite bodybuilders use glutamine in some form every day whether they train or not. So if  you want the highest-quality glutamine available, get Gaspari, pure, pharmaceutical grade Glutamine today!" ,
    price: 80,
    photo: "http://ecx.images-amazon.com/images/I/71hWoTRQwVL._SL1500_.jpg",
    form: "Powder",
    category: "Glutamine",
    brand: "Gaspari Nutrition",
    goal: "Get fit"
  },
  {
    name: "BSN Lean Dessert 630g",
    description: "Unlike most proteins you may have likely sampled in the past, Lean Dessert Protein is not made up of one single source of protein (i.e. just whey or just egg). Rather, Lean Dessert Protein is an ultra-premium blend of 6 of the highest quality and most nutritionally complete proteins available, all with varying amino acid profiles and digestive rates. Since these individual proteins digest and reabsorb in the body at different rates, you are feeding your body for up to 7 hours after each shake. This should definitely help to subside hunger. In addition to the sustained-release blend, Lean Dessert Protein mixes up instantly to produce a thick, rich shake, as opposed to a thin “watery” shake that is sometimes the case with other protein powders. This will also keep you feeling fuller, without causing any digestive discomfort or bloating." ,
    price: 240,
    photo: "http://images.iherb.com/l/BSN-00607-1.jpg",
    form: "Powder",
    category: "Protein",
    brand: "BSN",
    goal: "Gain lean muscle"
  },
  {
    name: "Optimum Nutrition ZMA 90ct",
    description: "Two recent clinical trials have shown that a synergistic combination of Zinc Monomethionine Aspartate, Magnesium Aspartate, and Vitamin B6 can significantly increase anabolic hormone levels and muscle strength in well-trained athletes. The novel Zinc Monomethionine Aspartate formula may also help to increase endurance, healing, growth and restful sleep." ,
    price: 120,
    photo: "http://www.thesupplementslab.com.au/product_images/733-The%20Supplements%20Lab%20Optimum%20Nutrition%20ZMA.jpg",
    form: "Capsules",
    category: "Testosteron Boosters",
    brand: "Muscle Tech",
    goal: "Gain weight"
  },
  {
    name: "MET-Rx Xtreme Multi-Vitamins 50ct",
    description: "MET-Rx Xtreme Multi Vitamins contains 36 vitamins, minerals, trace elements and other important nutrients, enabling you to get a daily supply of nutrients that you may not be obtaining from your diet. Multivitamin and mineral supplements are perhaps the most important single supplement that can be consumed by bodybuilders and athletes because if a deficiency does exist, it may restrict your ability to grow and recover in response to exercise. MET-Rx Xtreme Multi Vitamins delivers a daily dose of a vast number of nutrients. In general, a good daily multivitamin/mineral supplement improves your overall bodily functioning and boosts both physical and mental health and wellbeing. The individual vitamins and nutrients in a quality daily multivitamin dose deliver the specific benefits of each individual vitamin, plus you get the combined, or synergistic, benefits of all the ingredients working together." ,
    price: 50,
    photo: "http://www.metrxshop.co.uk/ekmps/shops/usnutrition/images/met-rx-xtreme-multivitamins-7-p.jpg",
    form: "Pills",
    category: "Vitamines & Minerals",
    brand: "Xtreme",
    goal: "Loose weight"
  },
  {
    name: "Boditronics Mass Attack Heavyweight 6kg",
    description: "Some people have a very fast metabolism, which means it is physically very difficult to eat enough without feeling sick or bloated in order to gain weight. Mass Attack Heavyweight is specifically designed for \"hardgainers\". Containing a massive 60g of muscle-building protein, together with quality complex carbohydrates and medium chain triglycerides (a fat that is processed in the body like a carbohydrate). Mass Attack Heavyweight packs a monster 1300 calories per serving when mixed with water (more if mixed with milk). This potent blend of nutrients and energy is enough to pack muscle on the most stubborn frame. In addition, Mass Attack Heavyweight contains the heavyweight gain stack, a precise mix of Creatine monohydrate and dextrose monohydrate designed to transport Creatine to the muscles quickly and efficiently. Continued supplementation with Creatine has been shown to increase power and muscle size and strength. Unlike most high calorie mass gain products, Mass Attack Heavyweight mixes easily in a shaker and is not overly thick, making it easy to consume. Available in delicious flavours, Mass Attack Heavyweight contains no artificial colours, flavours or preservatives and is unique in that it also does not contain any artificial sweeteners.",
    price: 300,
    photo: "http://ecx.images-amazon.com/images/I/81r5MzCktEL._SL1500_.jpg",
    form: "Powder",
    category: "Weight gainers",
    brand: "Boditronics",
    goal: "Longevity",
  },
  {
    name: "Reflex Thermo Fusion 100ct",
    description: "Thermo Fusion sets a completely new benchmark in fat loss formulae. It’s guaranteed to be the most effective legal weight management formula available. Thermo Fusion contains perfectly safe and scientifically proven ingredients to help you get the results you want faster when combined with an effective training and dietary regime. Each serving of Thermo Fusion contains a potent industry leading dose of of L-Carnitine, Green Tea extract, Caffeine, Choline, L-Phenylalanine, Cayenne Pepper, ALA, Bioperine and Chromium. Thermo Fusion is made in Reflex Nutrition's own state of the art factory. Other diets pills are often contract packed adding a large chunk to the overall cost of the product, often making the product either expensive or poor in terms of ingredients and dosages. Precise control over manufacturing and selection of the finest ingredients are often recipes for success. That’s why quality control at Reflex Nutrition is so important. Thermo Fusion is made in accordance with very strict ISO9001 quality control procedures and only contains the purest, highest quality ingredients available." ,
    price: 120,
    photo: "http://www.phd-fitness.co.uk/store/products/large/1344258538_dietproteinwiththermofusion.JPG",
    form: "Capsules",
    category: "Fat burners",
    brand: "Reflex",
    goal: "Faster recovery"
  },
  {
    name: "CNP Professional Pro Creatine E2 240ct",
    description: "CNP Professional's Pro-Creatine E2 ethyl ester is creatine at the end stage of evolution. By bonding an ester to creatine, CNP have given it a lipophilic (fat penetrating) property. This has increased its bio availability over any other form of creatine by a staggering 3900%! This now means that any fluid retained will be in muscle cells, not under the skin. Dosages need only be a small percentage of conventional dosing protocols and there is no need to load and no down-regulation of creatine transporters. There's also no need to add simple sugars to create an \"insulin spike\", thus none of the potential gains in bodyfat associated with this approach. Best of all, there are none of the unpleasant side effects or damage to performance that can be experienced with existing forms of creatine. Now, all that can be gained from creatine supplementation is benefit." ,
    price: 240,
    photo: "http://monstersupplements.com/store/products/large/1270046410_Procreatine%20E2%2072dpi.jpg",
    form: "Liquid",
    category: "Creatine",
    brand: "CNP",
    goal: "Fat loss"
  },
  {
    name: "ISS Research L-Glutamine Power 1kg",
    description: "ISS Research is committed to helping you achieve your true athletic potential. That's why ISS is proud to bring you Complete L-Glutamine Power. Glutamine, the most abundant single amino acid in the muscle tissue, is becoming more prominent as research reveals it's effects on: prevention of muscle breakdown (anticatabolic), protein synthesis, increased GH levels, and supporting healthy immune system function. Use glutamine daily before and after workouts when concentrations are low and recovery is a must.ISS Research's Complete L-Glutamine Power is 100% pure grade and is free of contaminants and impurities. " ,
    price: 200,
    photo: "http://ecx.images-amazon.com/images/I/81qd0QmPgoL._SL1500_.jpg",
    form: "Powder",
    category: "Special Offers",
    brand: "ISS",
    goal: "Gain lean muscle"
  },
  {
    name: "Universal Nutrition Prostate Support 60ct",
    description: "Athletes who use anabolic prohormones regularly or older men stand to benefit from Universal Nutrition's Prostate Support. For these individuals, dihydrotestosterone (DHT) poses special male-related problems. This metabolite of testosterone has been associated with a number of symptoms including prostate enlargement and accelerated male pattern baldness. Universal Nutrition's Prostate Support, with its comprehensive blend of select botanicals, amino acids, vitamins and minerals–twelve ingredients in all–can help. By inhibiting the activity of 5-alpha-reductase, you reduce the likelihood of DHT conversion. So if you’re training hard or over the age of 40, consider your health. Consider Prostate Support." ,
    price: 135,
    photo: "http://media.astronutrition.com/media/catalog/product/cache/6/image/5e06319eda06f020e43594a9c230972d/f/i/file_25_32/astronutrition.com-Universal-Nutrition-Prostate-Support---60-tabs-31.jpg",
    form: "Caps",
    category: "Special Offers",
    brand: "Universal Nutrition",
    goal: "Longevity"
  }
]

# products = [
#   {
#     :name => "Ruby on Rails Tote",
#     :tax_category => food,
#     :shipping_category => shipping_category,
#     :price => 15.99,
#     :eur_price => 14,
#   }
# ]

products.each do |product_attrs|
  product = Spree::Product.new

  # Create base product
  product.name = product_attrs[:name]
  product.description = product_attrs[:description]
  product.price = product_attrs[:price]
  product.tax_category = food
  product.shipping_category = default_shipping_category
  product.available_on = Time.zone.now

  [:form, :brand, :category, :goal].each do |taxon_name|
    taxon = Spree::Taxon.find_by_name!(product_attrs[taxon_name])
    product.taxons << taxon
  end

  product.save!

  # Create variants
  Spree::OptionType.find_by_presentation!("Size").option_values.each do |option_value|
    variant = product.variants.new
    variant.option_values = [option_value]
    variant.sku = "SLUG-#{rand(1...1000)}"
    variant.save!

    image = variant.images.new
    image.attachment = URI.parse(product_attrs[:photo])
    image.save!
  end

end

Spree::Variant.all.each do |variant|
  variant.stock_items.each do |stock_item|
    Spree::StockMovement.create(:quantity => 10, :stock_item => stock_item)
  end
end
