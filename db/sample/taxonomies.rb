taxonomies = [
  { :name => "Categories" },
  { :name => "Brand" },
  { :name => "Goal" },
  { :name => "Form" }
]

taxonomies.each do |taxonomy_attrs|
  Spree::Taxonomy.create!(taxonomy_attrs)
end