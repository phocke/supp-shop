taxons = [
  {
    :taxonomy => "Categories",
    :taxons => ["BCAA & Amino Acids", "Glutamine", "Protein",
      "Testosteron Boosters", "Vitamines & Minerals", "Weight gainers",
      "Fat burners", "Creatine", "Special Offers"]
  },
  {
    :taxonomy => "Form",
    :taxons => ["Powder", "Capsules", "Pills", "Liquid", "Caps"]
  },
  {
    :taxonomy => "Brand",
    :taxons => ["Universal Nutrition", "ISS", "CNP", "Reflex",
      "Boditronics", "Xtreme", "Muscle Tech", "BSN", "Gaspari Nutrition",
      "Optimum Nutrition"]
  },
  {
    :taxonomy => "Goal",
    :taxons => ["Loose weight", "Get fit", "Gain lean muscle", "Gain weight",
      "Longevity", "Faster recovery", "Fat loss"]
  }
]

taxons.each_with_index do |attrs, i|
  taxonomy = Spree::Taxonomy.find_by_name!(attrs[:taxonomy])

  attrs[:taxons].each_with_index do |name, idx|
    taxon = Spree::Taxon.new
    taxon.name = name
    taxon.taxonomy = taxonomy
    taxon.parent = Spree::Taxon.find_by_name!(attrs[:taxonomy])
    taxon.position = idx
    taxon.save!
  end

end