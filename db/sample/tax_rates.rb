north_america = Spree::Zone.find_by_name!("North America")

food = Spree::TaxCategory.find_by_name!("Food")

tax_rate = Spree::TaxRate.new
tax_rate.name = "North America"
tax_rate.zone = north_america
tax_rate.amount = 0.05
tax_rate.tax_category = food

tax_rate.calculator = Spree::Calculator::DefaultTax.create!
tax_rate.save!