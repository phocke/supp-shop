Spree::Core::Engine.load_seed if defined?(Spree::Core)
Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

# Keep the default samples for shipping and payments
Spree::Sample.load_sample("payment_methods")
Spree::Sample.load_sample("shipping_categories")
Spree::Sample.load_sample("shipping_methods")

puts "Loading sample data specific to SuppShop"
require Rails.root.join('db', 'sample', 'tax_categories')
require Rails.root.join('db', 'sample', 'tax_rates')
require Rails.root.join('db', 'sample', 'option_types')
require Rails.root.join('db', 'sample', 'option_values')

require Rails.root.join('db', 'sample', 'taxonomies')
require Rails.root.join('db', 'sample', 'taxons')

require Rails.root.join('db', 'sample', 'products')

require Rails.root.join('db', 'sample', 'paypal')

# Spree::Sample.load_sample("products")
# Spree::Sample.load_sample("taxons")
# Spree::Sample.load_sample("option_values")
# Spree::Sample.load_sample("product_option_types")
# Spree::Sample.load_sample("product_properties")
# Spree::Sample.load_sample("prototypes")
# Spree::Sample.load_sample("variants")
# Spree::Sample.load_sample("stock")
# Spree::Sample.load_sample("assets")

# Spree::Sample.load_sample("orders")
# Spree::Sample.load_sample("adjustments")
# Spree::Sample.load_sample("payments")
