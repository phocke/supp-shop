accounting.settings =
  currency:
    symbol: "$"
    format: "%s%v"
    decimal: "."
    thousand: ","
    precision: 2
  number:
    precision: 0
    thousand: ","
    decimal: "."

window.m = (amount, cents = false)->

  options =
    precision: if cents then 2 else 0

  accounting.formatMoney(amount, options)