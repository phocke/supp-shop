ShopApp.Views.Main = React.createClass
  getInitialState: () ->
    @props.router.on 'changeView', @_changeView
    @_searchQueryModel = new Backbone.Model()

    {}

  _headSlideUp: () ->
    $('#header-top').animate('max-height': 0)
    $('.btn-down').addClass('view')
    $('.btn-down span').addClass('view')

  _headSlideDown: () ->
    $('#header-top').animate('max-height': 1000)
    $('.btn-down').removeClass('view')
    $('.btn-down span').removeClass('view')

  _changeView: ({nameContainsKeyword}) ->
    if nameContainsKeyword
      @_searchQueryModel.set 'nameContains', nameContainsKeyword

  render: ShopApp.Templates.Main
