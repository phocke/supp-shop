ShopApp.Views.AdvancedFilters = React.createClass
  getInitialState: ->
    @_filters = ShopApp.Models.Filters.getInstance()
    @_filters.set 'advancedOpened', false

    @_taxonomies = ShopApp.Models.Taxonomies.getInstance()
    @_taxonomies.on 'sync', @_taxonomiesSynced

    advancedOpened: false
    taxonomies: null

  componentWillUnmount: ->
    @_taxonomies.off 'sync', @_taxonomiesSynced

  _taxonomiesSynced: ->
    @setState
      taxonomies:
        form: @_taxonomies.getFormTaxonomy()
        categories: @_taxonomies.getCategoriesTaxonomy()
        brand: @_taxonomies.getBrandTaxonomy()

  _openAdvanced: -> @setState advancedOpened: true
  _closeAdvanced: -> @setState advancedOpened: false

  componentDidUpdate: ->
    @_filters.set 'advancedOpened', @state.advancedOpened

    openButton = $ @refs.openButton.getDOMNode()
    closeButton = $ @refs.closeButton.getDOMNode()
    filters = $(@refs.root.getDOMNode()).children('.filter')

    if @state.advancedOpened
      openButton.fadeOut 200
      closeButton.delay(800).fadeIn 200
      filters.delay(800).fadeIn 200
    else
      openButton.delay(800).fadeIn 200
      closeButton.fadeOut 200
      filters.fadeOut 200

  render: ShopApp.Templates.AdvancedFilters
