ShopApp.Views.AdvancedTaxonomyEntry = React.createClass
  getInitialState: ->
    @_filters = ShopApp.Models.Filters.getInstance()
    selected: false

  _toggleSelection: ->
    if @_filters.isTaxonInConjunction(@props.taxon)
      @_filters.removeTaxonFromConjunction @props.taxon
      @setState selected: false
    else
      @_filters.addTaxonToConjunction @props.taxon
      @setState selected: true

    false

  render: ShopApp.Templates.AdvancedTaxonomyEntry
