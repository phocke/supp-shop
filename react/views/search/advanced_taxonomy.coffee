ShopApp.Views.AdvancedTaxonomy = React.createClass
  getInitialState: ->
    @_filters = ShopApp.Models.Filters.getInstance()
    @_filters.on 'change:conjunctionTaxonList', () => @setState @_computeState()

    @_computeState()

  toggleListVisibility: () ->
    if @state.open
      @setState open: false

    else if (selectedTaxons = @_getSelectedTaxons()).length > 0
      # list is closed and taxon for this taxonomy is selected, we need to first
      # remove all selected taxons from this taxonomy, before opening the list
      @_filters.removeTaxonFromConjunction taxon for taxon in selectedTaxons

    else
      @setState open: true

    false


  _getSelectedTaxons: () ->
    @_filters.getTaxonsInConjunctionFor(@props.taxonomy)

  _computeState: () ->
    selectedTaxon: @_getSelectedTaxons()[0]
    open: false

  render: ShopApp.Templates.AdvancedTaxonomy
