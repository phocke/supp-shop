ShopApp.Views.SearchFilter = React.createClass
  getInitialState: ->
    @_filters = ShopApp.Models.Filters.getInstance()
    inputValue: @_filters.get('searchPhrase')

  onInputChange: (e) ->
    @_filters.setSearchPhrase e.target.value
    @setState inputValue: @_filters.get('searchPhrase')

  componentDidMount: ->
    @_filters.on 'change:advancedOpened', @advancedOpenedChanged

  componentDidUnmount: ->
    @_filters.off 'change:advancedOpened', @advancedOpenedChanged

  advancedOpenedChanged: ->
    input = $(@refs.input.getDOMNode())
    if @_filters.get('advancedOpened')
      input.animate({width: 340},400)
    else
      input.animate({width: 783},400)

  render: ShopApp.Templates.SearchFilter
