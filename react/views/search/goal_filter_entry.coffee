ShopApp.Views.GoalFilterEntry = React.createClass
  getInitialState: ->
    @_filters = ShopApp.Models.Filters.getInstance()

    selected: @_filters.isTaxonInAlternative(@props.taxon)

  _toggleSelected: ->
    if @state.selected
      @setState selected: false
      @_filters.removeTaxonFromAlternative @props.model
    else
      @setState selected: true
      @_filters.addTaxonToAlternative @props.model

  render: ShopApp.Templates.GoalFilterEntry
