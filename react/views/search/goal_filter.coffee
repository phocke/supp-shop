Taxonomies = ShopApp.Models.Taxonomies

ShopApp.Views.GoalFilter = React.createClass
  getInitialState: ->
    @_taxonomies = Taxonomies.getInstance()
    @_taxonomies.on 'sync', @_taxonomiesSynced

    taxons: null

  componentWillUnmount: ->
    @_taxonomies.off 'sync', @_taxonomiesSynced

  _taxonomiesSynced: ->
    @setState
      taxons: @_taxonomies.getGoalTaxonomy().get 'taxons'

  render: ShopApp.Templates.GoalFilter
