ShopApp.Views.Checkout = React.createClass

  getInitialState: () ->
    @_cart = ShopApp.Models.Cart.getInstance()

    # Setup fake email and address
    initState =
      step: 0
      order: {}
      email: "empty@example.com"
      errors: {}
      use_billing: true
      billing_address: {
        firstname: "John"
        lastname: "Smith"
        address1: "5th Avenue 25"
        city: "New York"
        zipcode: "15211"
        phone: "(123) 213-123-123"
      }
      shipping_address: {
        firstname: ""
        lastname: ""
        address1: ""
        city: ""
        zipcode: ""
        phone: ""
      }

  componentDidMount: () ->
    @_cart.on 'add remove reset change:quantity', @_cartChanged

  componentDidUpdate: ->
    @_showStep(@state.step)

  _cartChanged: () ->
    @setState(summedPrice: @_cart.sumPrices())

  onShippingRateChange: (evt) ->
    @setState shipping_rate_id: evt.target.value

  onEmailChange: (evt) ->
    @setState email: evt.target.value

  updateBillingAddress: (key, value) ->
    address = @state.billing_address
    address[key] = value

    @setState 'billing_address': address

  updateShippingAddress: (key, value) ->
    address = @state.shipping_address
    address[key] = value

    @setState 'shipping_address': address

  _continueShopping: ->
    @setState step: 0, order: {}

    $('body').toggleClass('view');
    $('#checkout').toggleClass('view');

  _emptyCart: ->
    @_cart.reset()
    @_continueShopping()

  _cancelUseBilling: (evt) ->
    @setState(use_billing: false)

    $('.show-bill').addClass('hidden')

    $('section[data-name="address"] form').fadeIn()

  # Before going to address tab we create order with current line items
  _submitStepLineItems: ->
    order = new ShopApp.Models.OrderModel(cart: @_cart)

    order.save({},
      data: 1
      success: (order)=>
        @setState(order: order.attributes, step: 1, errors: {})
    )

  _submitStepAddress: ->
    order = new ShopApp.Models.CheckoutModel(
      id: @state.order.number,
      number: @state.order.number
      order_token: @state.order.token,
      bill_address: @state.billing_address,
      ship_address: @state.shipping_address,
      use_billing: @state.use_billing,
      email: @state.email,
    )

    order.save({},
      data: 1,
      success: (model)=>

        proceed = (model)=>
          shipping_rates = model.attributes.shipments[0].shipping_rates

          @setState(
            step: 2,
            shipping_rate_id: shipping_rates[0].id,
            shipping_rates: shipping_rates,
            order: order.attributes,
            errors: { }
          )

        # If user has already gone throught this step (address submiting)
        # we do not have to call #nextStep one more time.
        #
        if model.attributes.state == "payment"
          proceed(model)
        else
          order.nextStep(
            success: proceed
            error: @_handleErorrs
          )
      error: @_handleErorrs
    )

  _submitStepDelivery: ->
    order = new ShopApp.Models.CheckoutModel(
      id: @state.order.number,
      number: @state.order.number
      order_token: @state.order.token,
      shipping_rate_id: @state.shipping_rate_id,
      shipment_id: @state.order.shipments[0].id,
    )

    order.save({},
      data: 1,
      success: (model)=>
        method = _.findWhere(model.attributes.payment_methods, {name: "PayPal"})
        @setState(payment_method: method.id, step: 3, order: order.attributes, errors: {})
      error: @_handleErorrs
    )

  _submitStepPayment: ->
    @setState(step: 4)

  _setStep: (step) ->
    @setState step: step

  _prevStep: () ->
    @_setStep(@state.step - 1)

  _goStepLineItems: () ->
    @_setStep(0) if @state.step > 0

  _goStepAddress: () ->
    @_setStep(1) if @state.step > 1

  _goStepDelivery: () ->
    @_setStep(2) if @state.step > 2

  _goStepPayment: () ->
    @_setStep(3) if @state.step > 3

  _nextStep: () ->
    switch @state.step
      when 0 then @_submitStepLineItems()
      when 1 then @_submitStepAddress()
      when 2 then @_submitStepDelivery()
      when 3 then @_submitStepPayment()

  _handleErorrs: (model, xhr, options)->
    data = xhr.responseJSON

    if data.error
      @setState errors: data.errors
    else
      @setState errors: {}

  _showStep: (n)->
    $root = $ @refs.root.getDOMNode()

    $("section[data-step]", $root).removeClass('active')
    $("section[data-step=#{n}]", $root).addClass('active')

    $("#checkout-navigation .step").removeClass('active').removeClass('next').removeClass('prev')
    $("#checkout-navigation .step").eq(n).addClass('active')
    $("#checkout-navigation .step").eq(n).prev('.step').addClass('prev')
    # $("#checkout-navigation .step").eq(n + 1).addClass('next')

  render: ShopApp.Templates.Checkout