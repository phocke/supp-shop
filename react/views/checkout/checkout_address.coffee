ShopApp.Views.CheckoutAddress = React.createClass

  getInitialState: () ->
    @_countries = new ShopApp.Models.Countries
    @_countries.fetch()

    initState =
      country: new ShopApp.Models.Country

  onAddressChange: (evt)->
    this.props.updateAddress(evt.target.getAttribute('data-name'), evt.target.value)

  onCountryChange: (evt) ->
    id = parseInt(evt.target.value, 10)

    this.props.updateAddress('country_id', id)

    country = new ShopApp.Models.Country(id: id)

    country.fetch(
      success: =>
        @setState country: country
    )

  render: ShopApp.Templates.CheckoutAddress