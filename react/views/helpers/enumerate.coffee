ShopApp.Views.Enumerate = React.createClass
  componentDidMount: () ->
    @props.collection.on 'add remove reset', @collectionChanged

  componentWillUnmount: () ->
    @props.collection.off 'add remove reset', @collectionChanged

  componentDidUpdate: () ->
    @props.onDidUpdate?()

  collectionChanged: () ->
    @forceUpdate()

  render: ->
    if @props.collection && @props.collection.length > 0
      modelViews = _.map @props.collection.models, (p) =>
        @props.modelView { key: p.cid, model: p }

      React.DOM.div null, modelViews
    else
      React.DOM.div null, @props.children