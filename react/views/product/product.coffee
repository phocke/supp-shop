ShopApp.Views.Product = React.createClass
  render: ShopApp.Templates.Product
  getInitialState: ->
    product = @props.model
    taxonomies = ShopApp.Models.Taxonomies.getInstance()
    taxons = [
      product.getTaxonFrom taxonomies.getBrandTaxonomy()
      product.getTaxonFrom taxonomies.getCategoriesTaxonomy()
      product.getTaxonFrom taxonomies.getFormTaxonomy()
    ].filter (t) => !!t

    {menuOpened: false, taxons}

  _onMouseEnter: (e) ->
    $target = $ @refs.root.getDOMNode()

    $target.find('.caption').animate({bottom: 0}, 200)
    $target.find('.front').children('img').animate({bottom: 20}, 200)

  _onMouseLeave: (e) ->
    @setState menuOpened: false
    $target = $ @refs.root.getDOMNode()

    $target.find('.caption').animate({bottom: -50}, 200)
    $target.find('.front').children('img').animate({bottom: 0}, 200)

  _largePreview: (e) ->
    @props.model.trigger 'preview', @props.model

  _cardToggle: (e) ->
    @setState menuOpened: !@state.menuOpened
