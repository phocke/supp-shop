ShopApp.Views.BuyButton = React.createClass
  componentWillMount: () ->
    @_cart = ShopApp.Models.Cart.getInstance()

  componentDidMount: () ->
    @_cart.on 'add remove', @_forceUpdate

  componentWillUnmount: () ->
    @_cart.off 'add remove', @_forceUpdate

  _addCartItem: () ->
    product = @props.product
    @_cart.add
      product: product
      variant: @_cart.getAvailableVariants(product)[0]

    false

  _forceUpdate: () -> @forceUpdate()

  render: ShopApp.Templates.BuyButton
