ShopApp.Views.BuyButtonVariant = React.createClass
  getInitialState: () ->
    choosingVariant: false
    quantityInputValue: @props.cartItem.get('quantity')

  componentWillMount: () ->
    @_cart = ShopApp.Models.Cart.getInstance()
    @props.cartItem.on 'change', @_forceUpdate

  componentWillUnmount: () ->
    @props.cartItem.off 'change', @_forceUpdate

  _toggleVariantMenu: () ->
    @setState choosingVariant: !@state.choosingVariant
    false

  _chooseVariant: (variant) ->
    @props.cartItem.set {variant}
    @setState choosingVariant: false
    false

  _onMouseLeave: () ->
    @setState choosingVariant: false
    false

  _removeCartItem: () ->
    @_cart.remove @props.cartItem

  _quantityInputChanged: (e) ->
    newQuantityValue = e.target.value
    unless isNaN (parsed = parseInt newQuantityValue)
      parsed = 1 if parsed < 0
      parsed = 99 if parsed > 99

      if parsed == @props.cartItem.get('quantity')
        @setState quantityInputValue: parsed
      else
        @props.cartItem.set quantity: parsed
    else
      @setState quantityInputValue: newQuantityValue

  _quantityInc: () ->
    @props.cartItem.add 1
    false

  _quantityDec: () ->
    @props.cartItem.add -1
    false

  _forceUpdate: () ->
    return unless @isMounted()
    @setState quantityInputValue: @props.cartItem.get('quantity')

  render: ShopApp.Templates.BuyButtonVariant
