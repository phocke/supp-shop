$ = jQuery

ShopApp.Views.ProductPreview = React.createClass
  componentDidMount: () ->
    @props.products.on 'preview', @_previewRequested

  componentWillUnmount: () ->
    @props.products.off 'preview', @_previewRequested

  _previewRequested: (product) ->
    taxonomies = ShopApp.Models.Taxonomies.getInstance()
    taxons = [
      product.getTaxonFrom taxonomies.getBrandTaxonomy()
      product.getTaxonFrom taxonomies.getCategoriesTaxonomy()
      product.getTaxonFrom taxonomies.getFormTaxonomy()
    ].filter (t) => !!t

    @setState { product, taxons, active: true }

  _close: () ->
    @setState { active: false }

  componentDidUpdate: () ->
    if @state?.product?
      $root = $ @refs.root.getDOMNode()

      $box = $root.find('.box')
      $box.css('width', 550 + $box.find('.image img').width())

      $desc = $root.find('.desc')
      $desc.css('height', 390 - $desc.siblings('.parameters').height())
      $desc.perfectScrollbar(wheelSpeed: 25)
    else
      $('body').removeClass('view')

  render: ShopApp.Templates.ProductPreview
