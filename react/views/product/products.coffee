ShopApp.Views.Products = React.createClass
  componentWillMount: () ->
    @_filters = ShopApp.Models.Filters.getInstance()

    @_cachedCollection = new ShopApp.Models.Products
    @_filters.setFullCollection @_cachedCollection
    @_taxonomies = ShopApp.Models.Taxonomies.getInstance()

    @_presentedCollection = @_filters.getFilteredCollection()

    async.parallel [
      (callback) =>
        @_cachedCollection.fetch()
        @_cachedCollection.once 'sync', () -> callback(null)
      (callback) =>
        @_taxonomies.fetch()
        @_taxonomies.once 'sync', () -> callback(null)
    ], @_productsDownloaded

  _productsDownloaded: () ->
    @_cachedCollection.trigger 'downloaded'
    $('body').addClass('products-downloaded')

  _runWookmark: () =>
    options =
      autoResize: true
      container: $('#items')
      offset: 30

    handler = $ '#tiles .item'
    $('#tiles')._imagesLoaded () ->
      handler.wookmark options

  render: ShopApp.Templates.Products
