ShopApp.Views.CartItem = React.createClass
  getInitialState: () ->
    image = @props.model.get("product").getImages()[0]

    quantityInputValue: @props.model.get('quantity')
    hovered: false
    wideImage: image.attachment_height / 360.0 * 100.0 > 68

  componentDidMount: () ->
    @_cart = ShopApp.Models.Cart.getInstance()
    @_cart.on 'add remove reset change:quantity', @_cartChanged

  componentWillUnmount: () ->
    @_cart.off 'add remove reset change:quantity', @_cartChanged

  componentDidUpdate: () ->
    # $root = $ @refs.root.getDOMNode()
    # $hover = $ @refs.hover.getDOMNode()
    # $right = $ @refs.right.getDOMNode()
    # $quantity = $ @refs.quantity.getDOMNode()
    # $rate = $ @refs.rate.getDOMNode()

    # $hover.clearQueue()
    # $right.clearQueue()
    # $quantity.clearQueue()
    # $rate.clearQueue()

    # if @state.hovered
    #   $hover.fadeIn 200
    #   $right.animate {width: 200}, 400
    #   $quantity.animate {width: 198}, 400
    #   $rate.animate {width: 198}, 400
    # else
    #   $hover.fadeOut 200
    #   $right.animate {width: 50}, 400
    #   $quantity.animate {width: 48}, 400
    #   $rate.animate {width: 48}, 400


  _quantityInputChanged: (e) ->
    newQuantity = e.target.value

    if !isNaN(parseInt(newQuantity, 10))
      @props.model.set 'quantity', parseInt(newQuantity, 10)
    else
      @setState
        quantityInputValue: newQuantity

  _quantityIncrease: () ->
    @props.model.add 1

  _quantityDecrease: () ->
    @props.model.add -1

  _onMouseEnter: (e) -> @setState {hovered: true}
  _onMouseLeave: (e) -> @setState {hovered: false}

  _cartChanged: () ->
    @setState
      quantityInputValue: @props.model.get('quantity')

  _remove: ->
    @props.model.set 'quantity', 0

  _preview: ->
    product = @props.model.get("product")
    product.trigger 'preview', product

  render: ShopApp.Templates.CartItem
