ShopApp.Views.Cart = React.createClass
  getInitialState: () ->
    @_cart = ShopApp.Models.Cart.getInstance()

    initState =
      opened: false
      locked: false

    #returns initState
    _.extend initState, @_buildCartState()

  _buildCartState: () ->
    summedPrice: @_cart.sumPrices()
    productsNumber: @_cart.countProducts()

  componentDidMount: () ->
    @_cart.on 'add remove reset change', @_cartChanged
    $(@refs.scrolls.getDOMNode()).perfectScrollbar
      wheelSpeed: 25,
      useBothWheelAxes: true

  componentWillUnmount: () ->
    @_cart.off 'add remove reset change', @_cartChanged

  componentDidUpdate: ->
    $root = $ @refs.root.getDOMNode()
    $root.clearQueue()

    # $(@refs.scrolls.getDOMNode()).perfectScrollbar 'update'

    if @state.opened
      $root.animate { bottom: 0 }, 200
      $root.addClass 'opened'
    else
      $root.animate { bottom: -140 }, 200
      $root.removeClass 'opened'

  _cartChanged: () ->
    @setState @_buildCartState()

  _onMouseEnter: -> @setState { opened: true } if not @state.opened
  _onMouseLeave: -> @setState { opened: false } if not @state.locked

  _lock: -> @setState { locked: true }; false
  _unlock: -> @setState { locked: false }; false

  _requestCheckout: ->
    $('body').toggleClass('view');
    $('#checkout').toggleClass('view');

  render: ShopApp.Templates.Cart
