ShopApp.Views.CartSumup = React.createClass
  getInitialState: () ->
    @_cart = ShopApp.Models.Cart.getInstance()
    @_buildState()

  _buildState: () ->
    counter: @_cart.reduce ((memo, cartItem) -> memo + parseInt(cartItem.get('variant').price, 10)), 0

  componentDidMount: () ->
    @_cart.on 'add', @_cartChanged
    @_cart.on 'remove', @_cartChanged

  componentWillUnmount: () ->
    @_cart.off 'add', @_cartChanged
    @_cart.off 'remove', @_cartChanged

  _cartChanged: () ->
    @setState @_buildState()

  render: ShopApp.Templates.CartSumup
