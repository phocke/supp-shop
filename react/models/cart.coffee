ShopApp = @ShopApp

class ShopApp.Models.CartItem extends Backbone.Model
  defaults:
    quantity: 1

  getPrice: () -> @getSinglePrice() * @get('quantity')

  getSinglePrice: () -> parseInt(@get('variant').get('price'), 10)

  add: (number) ->
    @set 'quantity', @get('quantity') + number

  getAvailableVariants: () ->
    @collection.getAvailableVariants @get('product')


class ShopApp.Models.Cart extends Backbone.Collection
  model: ShopApp.Models.CartItem

  initialize: () ->
    super(arguments...)

    @on 'add remove reset change:quantity', @_saveCart
    @on 'downloaded:products', @_productsDownloaded
    @on 'change:quantity', (item) =>
      if item.get('quantity') == 0
        @remove item

  findByVariant: (variant) -> @findWhere variant_id: variant.id

  findByProduct: (product) -> @where {product}

  getAvailableVariants: (product) ->
    availableVariants = product.getNamedVariants()

    # exclude all variants of this product from different cart items
    chosenVariants = @findByProduct(product).map (c) -> c.get('variant')
    _.difference availableVariants, chosenVariants

  countWithVariant: (variant) ->
    item = @findWhere({variant_id: variant.id})
    if item?
      item.get('quantity')
    else
      0

  addToCart: (variant) ->
    item = @findByVariant variant
    if item
      item.add 1
    else
      @add
        variant_id: variant.id
        variant: variant
        quantity: 1

  countProducts: ->
    @reduce (memo, item) ->
      memo + item.get('quantity')
    , 0

  sumPrices: ->
    @reduce (memo, item) ->
      memo + item.getPrice()
    , 0

  @_instance: null
  @getInstance: () ->
    @_instance = new ShopApp.Models.Cart unless @_instance?
    @_instance

  _saveCart: () ->
    items = _.map @models, (item) ->
      variant_id: item.get('variant').id,
      product_id: item.get('product').id,
      quantity: item.get('quantity')

    localStorage.setItem('cart', JSON.stringify(items))

  _productsDownloaded: (products) ->
    items = JSON.parse(localStorage.getItem('cart'))

    _.map items, (item) =>
      product = products.findWhere(id: item.product_id)
      variant = product.get('variants').findWhere(id: item.variant_id)

      @add
        variant: variant
        product: product
        variant_id: item.variant_id
        quantity: item.quantity