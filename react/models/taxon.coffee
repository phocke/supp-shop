ShopApp = @ShopApp

class ShopApp.Models.Taxon extends Backbone.Model

class ShopApp.Models.Taxons extends Backbone.Collection
  model: ShopApp.Models.Taxon

  initialize: (_, {@taxonomy}) ->

  getTaxonIds: ->
    @map (taxon) -> taxon.get('id')
