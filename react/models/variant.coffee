ShopApp = @ShopApp

class ShopApp.Models.Variant extends Backbone.Model
  # TODO: rethink if should be memoized
  getVariantName: () ->
    @get('option_values').map((ov) -> ov.name).join(' - ')

  isNamed: () -> @get('option_values') > 0

class ShopApp.Models.Variants extends Backbone.Collection
  model: ShopApp.Models.Variant
