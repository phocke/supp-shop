ShopApp = @ShopApp

class ShopApp.Models.SearchQuery extends Backbone.Model
  @_instance: null
  @getInstance: () ->
    @_instance = new ShopApp.Models.SearchQuery unless @_instance?
    @_instance
