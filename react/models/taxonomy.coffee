ShopApp = @ShopApp

class ShopApp.Models.Taxonomy extends Backbone.Model
  parse: ({root}) ->
    root.taxons = new ShopApp.Models.Taxons(root.taxons, taxonomy: @)
    root

  _getTaxonIds: ->
    @get('taxons').getTaxonIds()

  _getTaxonById: (id) -> @get('taxons').findWhere {id}

  getMatchingTaxonFromList: (taxonIds) ->
    matchingId = _.intersection(taxonIds, @_getTaxonIds())[0]
    @_getTaxonById matchingId

class ShopApp.Models.Taxonomies extends Backbone.Collection
  url: -> '/api/taxonomies'
  model: ShopApp.Models.Taxonomy

  parse: (attrs) -> attrs.taxonomies

  @getInstance: ->
    @_instance ||= new ShopApp.Models.Taxonomies

  getBrandTaxonomy: -> @findWhere name: 'Brand'
  getCategoriesTaxonomy: -> @findWhere name: 'Categories'
  getFormTaxonomy: -> @findWhere name: 'Form'
  getGoalTaxonomy: -> @findWhere name: 'Goal'
