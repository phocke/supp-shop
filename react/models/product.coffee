ShopApp = @ShopApp

class ShopApp.Models.Product extends Backbone.Model
  urlRoot: -> "/api/products/"

  parse: (root) ->
    root.variants = new ShopApp.Models.Variants root.variants
    root

  getImages: () ->
    _.flatten @get('variants').map((v) -> v.get('images'))

  # this could be memoized
  getNamedVariants: () ->
    @get('variants').filter((v) -> v.get('option_values').length > 0)

  getDefaultNamedVariant: () ->
    @getNamedVariants()[0]

  getTaxonFrom: (taxonomy) ->
    taxonomy.getMatchingTaxonFromList @get('taxon_ids')

class ShopApp.Models.Products extends Backbone.Collection
  model: ShopApp.Models.Product
  url: -> '/api/products'
  parse: (data) ->
    data.products

  initialize: () ->
    super(arguments...)

    @on 'downloaded', @_downloaded, @

  findOrFetch: (id, callback) ->
    if product = @findWhere({id})
      setTimeout () ->
        callback product
      , 1
    else
      product = new ShopApp.Models.Product {id}
      product.fetch
        success: () -> callback(product)
        error: () -> callback(null)

  searchNameContains: (keyword) ->
    lowerKeyword = keyword.toLowerCase()
    filtered = @filter (product) -> product.attributes.name.toLowerCase().indexOf(lowerKeyword) >= 0

    new ShopApp.Models.Products filtered

  _downloaded: ->
    ShopApp.Models.Cart.getInstance().trigger('downloaded:products', @)