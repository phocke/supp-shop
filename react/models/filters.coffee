ShopApp = @ShopApp

delay = (time, fn) -> setTimeout fn, time

class ShopApp.Models.Filters extends Backbone.Model
  @_instance: null
  @getInstance: () ->
    @_instance = new ShopApp.Models.Filters unless @_instance?
    @_instance

  defaults:
    # if empty allow all, otherwise only these with all listed taxon ids
    alternativeTaxonList: []
    conjunctionTaxonList: []
    searchPhrase: ''

  initialize: ->
    @_filteredCollection = new ShopApp.Models.Products
    @on 'change:alternativeTaxonList', @_refreshFilteredCollection, @
    @on 'change:conjunctionTaxonList', @_refreshFilteredCollection, @
    @on 'change:searchPhrase', @_refreshFilteredCollection, @

  setFullCollection: (fullCollection) ->
    if @_fullCollection
      @_fullCollection.off 'downloaded', @_refreshFilteredCollection, @

    @_fullCollection = fullCollection
    @_fullCollection.on 'downloaded', @_refreshFilteredCollection, @

    if @_fullCollection.length > 0
      delay 0, @_refreshFilteredCollection()

  getFilteredCollection: () -> @_filteredCollection

  _refreshFilteredCollection: ->
    @_updateFilteredCollection @_getAcceptedProducts()

  _getAcceptedProducts: () ->
    models = @_filterByGoalTaxonId @_fullCollection.models
    models = @_filterByConjunctionTaxonList models
    models = @_filterBySearchPhrase models

  _filterBySearchPhrase: (models) ->
    return models if @get('searchPhrase').length == 0

    keywords = @get('searchPhrase').split(' ')
    keywords = _.map keywords, (keyword) -> keyword.toLowerCase()

    # underscore's filter returns accepted entitites
    _.filter models, (model) ->
      name = model.get('name').toLowerCase()
      _.every keywords, (keyword) -> name.indexOf(keyword) >= 0

  _updateFilteredCollection: (models) ->
    # react enforces full refresh during an event, but setting collection
    # triggers many 'added' and 'removed' events
    @_filteredCollection.set models, {silent: true}
    @_filteredCollection.trigger 'reset', @_filteredCollection


################################################################################

  isTaxonInAlternative: (taxon) ->
    _.contains @get('alternativeTaxonList'), taxon

  addTaxonToAlternative: (taxon) ->
    taxonList = @get 'alternativeTaxonList'
    unless _.contains taxonList, taxon
      taxonList = _.clone taxonList
      taxonList.push taxon
      @set 'alternativeTaxonList', taxonList

  removeTaxonFromAlternative: (taxon) ->
    taxonList = @get 'alternativeTaxonList'
    if _.contains taxonList, taxon
      taxonList = _.without taxonList, taxon
      @set 'alternativeTaxonList', taxonList

  _filterByGoalTaxonId: (models) ->
    return models if @get('alternativeTaxonList').length == 0

    alternativeTaxonIdList = _.map(
      @get('alternativeTaxonList'),
      (t) -> t.get('id')
    )

    _.filter models, (model) =>
      _.intersection(
        alternativeTaxonIdList,
        model.get('taxon_ids')
      ).length > 0


################################################################################

  isTaxonInConjunction: (taxon) ->
    _.contains @get('conjunctionTaxonList'), taxon

  # check if any taxon in conjunction is in the taxonomy
  getTaxonsInConjunctionFor: (taxonomy) ->
    _.intersection(
      @get('conjunctionTaxonList'),
      taxonomy.get('taxons').models
    )

  addTaxonToConjunction: (taxon) ->
    taxonList = @get 'conjunctionTaxonList'
    unless _.contains taxonList, taxon
      taxonList = _.clone taxonList
      taxonList.push taxon
      @set 'conjunctionTaxonList', taxonList

  removeTaxonFromConjunction: (taxon) ->
    taxonList = @get 'conjunctionTaxonList'
    if _.contains taxonList, taxon
      taxonList = _.without taxonList, taxon
      @set 'conjunctionTaxonList', taxonList

  _filterByConjunctionTaxonList: (models) ->
    return models if @get('conjunctionTaxonList').length == 0

    conjunctionTaxonIdList = _.map(
      @get('conjunctionTaxonList'),
      (t) -> t.get('id')
    )

    _.filter models, (model) =>
      _.intersection(
        model.get('taxon_ids'),
        conjunctionTaxonIdList
      # all conjunction taxons must be in model to pass
      ).length == conjunctionTaxonIdList.length


################################################################################

  setSearchPhrase: (phrase) -> @set 'searchPhrase', phrase
