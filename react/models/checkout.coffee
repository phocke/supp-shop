ShopApp = @ShopApp

class ShopApp.Models.CheckoutModel extends Backbone.Model

  urlRoot: '/api/checkouts'

  getDefaultParams: ->
    {
      "order_token": @attributes.order_token
    }

  url: ->
    params = @getDefaultParams()

    if @attributes.shipping_rate_id
      params[ "order[shipments_attributes][0][selected_shipping_rate_id]" ] = @attributes.shipping_rate_id
      params[ "order[shipments_attributes][0][id]"] = @attributes.shipment_id

    else
      params[ "order[email]" ] = @attributes.email
      params[ "order[use_billing]" ] = @attributes.use_billing

      _.each @attributes.bill_address, (value, key)->
        params[ "order[bill_address_attributes][#{key}]" ] = value

      unless @attributes.use_billing
        _.each @attributes.ship_address, (value, key)->
          params[ "order[ship_address_attributes][#{key}]" ] = value

    urlRoot = _.result(this, 'urlRoot')

    "#{urlRoot}/#{@attributes.number}?#{$.param(params)}"

  nextStep: (options)->
    params = $.param(@getDefaultParams())
    urlRoot = _.result(this, 'urlRoot')

    @save(
      {},
      url: "#{urlRoot}/#{@attributes.number}/next?#{params}",
      data: 1,
      success: options.success
      error: options.error
    )