ShopApp = @ShopApp

class ShopApp.Models.Country extends Backbone.Model
  urlRoot: -> "/api/countries"

class ShopApp.Models.Countries extends Backbone.Collection
  model: ShopApp.Models.Country
  url: -> '/api/countries?per_page=1000'
  parse: (data) ->
    data.countries