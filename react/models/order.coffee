ShopApp = @ShopApp

class ShopApp.Models.OrderModel extends Backbone.Model

  urlRoot: '/api/orders'
  url: ->
    params = {}

    _.each @attributes.cart.models, (model, idx)=>
      params[ "order[line_items][#{idx}][variant_id]" ] = model.get("variant").id
      params[ "order[line_items][#{idx}][quantity]" ] = model.get("quantity")

    "#{_.result(this, 'urlRoot')}?#{$.param(params)}"