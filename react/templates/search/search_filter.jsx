/** @jsx React.DOM */
ShopApp.Templates.SearchFilter = function() {
  return <form>
    <input
      type="text"
      placeholder="I’m looking for..."
      ref="input"
      value={this.state.inputValue}
      onChange={this.onInputChange} />
  </form>;
};
