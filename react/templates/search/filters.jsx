/** @jsx React.DOM */
ShopApp.Templates.Filters = function() {
  var AdvancedFilters = ShopApp.Views.AdvancedFilters;
  var GoalFilter = ShopApp.Views.GoalFilter;

  return <div class="container" id="filters">
    <AdvancedFilters />
    <GoalFilter filters={this._filters} />
  </div>;
};
