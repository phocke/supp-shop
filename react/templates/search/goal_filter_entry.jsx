/** @jsx React.DOM */
ShopApp.Templates.GoalFilterEntry = function() {
  var highlighted = this.state.selected || this.state.active;

  var taxon = this.props.model;
  var taxon_slug = slug(taxon.attributes.name);

  return <div className={"span2" + (this.state.selected ? " chosen" : "")}
    onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave}
    onClick={this._toggleSelected}>

    <img src={highlighted ? "/assets/" + taxon_slug + "_act.png" : "/assets/" + taxon_slug + ".png"} alt=""/>
    <span>
      {taxon.get('name')}
      {this.state.selected ?
        <i class="icon-ok circle"></i> :
      ''}
    </span>


  </div>;
};
