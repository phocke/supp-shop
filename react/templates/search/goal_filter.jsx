/** @jsx React.DOM */
ShopApp.Templates.GoalFilter = function() {
  var that = this;

  var GoalFilterEntry = ShopApp.Views.GoalFilterEntry;

  return <div id="goals" class="row">
    <h2 class="span3 text-center">I want to...</h2>

    {this.state.taxons ?
      this.state.taxons.first(5).map(function (taxon) {
        return <GoalFilterEntry
          model={taxon}
          key={taxon.cid}
          filters={that.props.filters} />;
      })
    : ''}
  </div>;
};
