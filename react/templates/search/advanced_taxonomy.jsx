/** @jsx React.DOM */
ShopApp.Templates.AdvancedTaxonomy = function() {
  var SearchFilter = ShopApp.Views.SearchFilter;
  var AdvancedTaxonomyEntry = ShopApp.Views.AdvancedTaxonomyEntry;

  return <div class={"btn-group filter text-left" +
    (this.state.selectedTaxon ? ' selected' : '') +
    (this.state.open ? ' open' : '')}
    ref="root"
    onClick={this.toggleListVisibility}>

    <a class="btn" href="">
      { this.state.selectedTaxon ?
        this.state.selectedTaxon.get('name') :
        this.props.taxonomy.get('name')
      }

      { this.state.selectedTaxon ?
        <i class="icon-remove circle"></i> :
        <i class="icon-caret-down circle"></i>
      }
    </a>

    <ul class="dropdown-menu">
      <span class="arrow"></span>
      {this.props.taxonomy.get('taxons').map(function (taxon) {
        return <AdvancedTaxonomyEntry taxon={taxon} />
      })}
    </ul>
  </div>;
};
