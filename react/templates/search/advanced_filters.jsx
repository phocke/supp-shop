/** @jsx React.DOM */
ShopApp.Templates.AdvancedFilters = function() {
  var SearchFilter = ShopApp.Views.SearchFilter;
  var AdvancedTaxonomy = ShopApp.Views.AdvancedTaxonomy;

  return <div id="search" class="row view" ref="root">
    <SearchFilter filters={this._filters} />

    <div class="trigger"
      onClick={this._openAdvanced} ref="openButton">
      <i class="icon-caret-left circle"></i>
      <span>Show advanced filters</span>
    </div>

    <div class="trigger view"
      onClick={this._closeAdvanced} ref="closeButton">
      <i class="icon-caret-left circle"></i>
    </div>

    {this.state.taxonomies ? <AdvancedTaxonomy taxonomy={this.state.taxonomies.form} /> : ''}
    {this.state.taxonomies ? <AdvancedTaxonomy taxonomy={this.state.taxonomies.categories} /> : ''}
    {this.state.taxonomies ? <AdvancedTaxonomy taxonomy={this.state.taxonomies.brand} /> : ''}
  </div>;
};
