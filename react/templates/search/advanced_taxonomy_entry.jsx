/** @jsx React.DOM */
ShopApp.Templates.AdvancedTaxonomyEntry = function() {
  var SearchFilter = ShopApp.Views.SearchFilter;

  return <li>
    <a href="#" onClick={this._toggleSelection}>
      {this.props.taxon.get('name')}
      {this.state.selected ?
        <i class="icon-ok icon-selected"></i>
      : ''}
    </a>
  </li>;
};
