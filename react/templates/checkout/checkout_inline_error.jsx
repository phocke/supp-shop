/** @jsx React.DOM */
ShopApp.Templates.CheckoutInlineError = function() {
  return <span class="inline-error">
    {this.props.errors[this.props.errorsScope + '.' + this.props.field ]}
  </span>;
}