/** @jsx React.DOM */
ShopApp.Templates.Checkout = function () {

  var InlineError = ShopApp.Views.CheckoutInlineError;
  var Address = ShopApp.Views.CheckoutAddress;

  var LineItems = _.map(this._cart.models, function(p) {
    return ShopApp.Views.CheckoutLineItem({ key: p.cid, model: p });
  });

  var LineItemsSummary = _.map(this._cart.models, function(p) {
    return ShopApp.Views.CheckoutLineItem({ key: p.cid, model: p });
  });

  if(this.state.shipping_rates && this.state.shipping_rates.length > 0) {
    var ShippingRatesOptions = _.map(this.state.shipping_rates, function(s) {
      return <option key={s.id +'rate'} value={s.id}>{s.name} ({s.display_cost})</option>
    });
  }

  if(this.state.shipping_rates) {
    var ShippingRates = <select name="shipping_rate_id" onChange={this.onShippingRateChange}>
      {ShippingRatesOptions}
    </select>
  }

  if(this.state.errors.base) {
    var BaseErrors = <div class="alert">
      {this.state.errors.base}
    </div>;
  } else {
    var BaseErrors = "";
  }

return <div id="checkout" class="text-center" ref="root">
    <header>
      <div class="container">
        <h2>Checkout</h2>
        <span class="total">total: {m(this.state.summedPrice)}</span>
        <span class="hidden-phone"></span>
        <i class="icon-remove circle toggle-checkout" onClick={this._continueShopping}></i>
      </div>
    </header>

    <div class="container steps">
        <section data-name="order-summary" class="active" data-step="0">
          <div class="step-content">
            <table class="table">
              <thead>
                <tr>
                  <th class="remove">
                  </th>
                  <th class="name" colSpan="2">item</th>
                  <th class="price">price</th>
                  <th class="qty">qty</th>
                  <th class="total">total</th>
                </tr>
              </thead>
              <tbody>
                {LineItems}
              </tbody>
            </table>
          </div>
        </section>

        <section data-name="address" data-step="1">
          <div class="step-content">
            <div class="left-col">
              <h4>Billing Adress</h4>
              {BaseErrors}
              <Address errors={this.state.errors}
                           errorsScope="bill_address"
                           address={this.state.billing_address}
                           updateAddress={this.updateBillingAddress} />
              // <form action="index_submit" method="get" accept-charset="utf-8">
              //   <p class="firstname">
              //       <input type="text" name="firstname" id="firstname" placeholder="First Name" />
              //   </p>
              //   <p class="lastname error">
              //       <input type="text" name="lastname" id="lastname" placeholder="Last Name" />
              //       <span>This field is required</span>
              //   </p>
              //   <p class="name">
              //       <input type="text" name="name" id="name" placeholder=". . ." />
              //   </p>
              // </form>
            </div>
            <div class="right-col">
              <h4>Shipping Address</h4>

              <div class="show-bill" onClick={this._cancelUseBilling}>
                <span>Use Billing Address</span><i class="icon-remove circle toggle-checkout"></i>
              </div>

              <Address errors={this.state.errors}
                       errorsScope="ship_address"
                       address={this.state.shipping_address}
                       updateAddress={this.updateShippingAddress} />

              // <form action="index_submit" method="get" accept-charset="utf-8">
              //   <p class="firstname">
              //       <input type="text" name="firstname" id="firstname" placeholder="First Name" />
              //   </p>
              //   <p class="lastname">
              //       <input type="text" name="lastname" id="lastname" placeholder="Last Name" />
              //   </p>
              //   <p class="name">
              //       <input type="text" name="name" id="name" placeholder=". . ." />
              //   </p>
              // </form>
            </div>
          </div>
        </section>

        <section data-name="delivery" data-step="2">
          <div class="step-content">
            <div class="radios clearfix">
              {ShippingRates}
            </div>
            <table class="table">
              <thead>
                <tr>
                  <th class="remove">
                  </th>
                  <th class="name" colspan="2">item</th>
                  <th class="price">price</th>
                  <th class="qty">qty</th>
                  <th class="total">total</th>
                </tr>
              </thead>
              <tbody>
                {LineItemsSummary}
              </tbody>
            </table>
          </div>
        </section>

        <section data-name="payment" data-step="3">
          <div class="step-content">
            <a href={"/paypal?payment_method_id=" + this.state.payment_method + "&order_token=" + this.state.order.token} data-method="post" id="paypal_button" rel="nofollow">
              <img alt="Btn_xpresscheckout" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" />
            </a>
                // <div class="credit-card">
                //     <img src="" />
                //     <img src="" />
                // </div>
                // <div class="credit-card">
                //     <img src="" />
                //     <img src="" />
                // </div>
                // <div class="credit-card">
                //     <img src="" />
                //     <img src="" />
                // </div>
                // <div class="credit-card">
                //     <img src="" />
                //     <img src="" />
                // </div>
                // <div class="form">
                //     <div>
                //       <input type="text" name="number" id="number" placeholder="Card Number" />
                //       <img src="" alt="Credit Card Explenation" class="number" />
                //     </div>
                //     <div>
                //       <input type="text" name="date" id="date" placeholder="MM/YY" />
                //       <img src="" alt="Credit Card Explenation" />
                //     </div>
                //     <div>
                //       <input type="text" name="code" id="code" placeholder="CVC" />
                //       <img src="<!-- @path cd-sacurity.png -->" alt="Credit Card Explenation" />
                //     </div>
                // </div>
          </div>
        </section>

        <section data-name="complete" data-step="4">
          <div class="step-content">
            <p>Done!</p>
          </div>
        </section>

    </div>

    <div id="checkout-navigation">
        <i class="icon-caret-left circle prev" onClick={this._prevStep}></i>
        <div class="step active" onClick={this._goStepLineItems}>
            1<span class="hidden-phone">. Order Summary</span>
        </div>
        <div class="step" onClick={this._goStepAddress}>
            2<span class="hidden-phone">. Adress</span>
        </div>
        <div class="step" onClick={this._goStepDelivery}>
            3<span class="hidden-phone">. Delivery</span>
        </div>
        <div class="step" onClick={this._goStepPayment}>
            4<span class="hidden-phone">. Payment</span>
        </div>
        <div class="step">
            5<span class="hidden-phone">. Complete</span>
        </div>
        <i class="icon-caret-right circle next" onClick={this._nextStep}></i>
    </div>
  </div>;
}