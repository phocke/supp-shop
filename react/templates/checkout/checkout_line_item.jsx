/** @jsx React.DOM */
ShopApp.Templates.CheckoutLineItem = function() {
  return <tr>
    <td class="remove"><i class="icon-remove circle" onClick={this._remove}></i></td>
    <td class="image">
      <img src={this.props.model.get("product").getImages()[0].small_url} alt="" />
    </td>
    <td class="name">
      <h3>{this.props.model.get("variant").get("name")}</h3>
      <span class="hidden-phone">{this.props.model.get("variant").get("description")}</span>
    </td>
    <td class="price"><h4>{m(this.props.model.getSinglePrice())}</h4></td>
    <td class="qty"><h4>{this.props.model.get('quantity')}</h4></td>
    <td class="total"><h4>{m(this.props.model.getPrice())}</h4></td>
  </tr>;
}