/** @jsx React.DOM */
ShopApp.Templates.CheckoutAddress = function() {

  var InlineError = ShopApp.Views.CheckoutInlineError;

  var CountriesOptions = _.map(this._countries.models, function(c) {
    return <option value={c.attributes.id}>{c.attributes.name}</option>
  });

  var StatesOptions = _.map(this.state.country.attributes.states, function(s) {
    return <option value={s.id}>{s.name}</option>
  });

  if(this.state.country.attributes.states && this.state.country.attributes.states.length > 0) {
    var StatesInput = <select onChange={this.onAddressChange} data-name="state_id">
      <option>Select state</option>
      {StatesOptions}
    </select>;
  } else {
    var StatesInput = <input type="text" data-name="state_name" placeholder="State" onChange={this.onAddressChange} />;
  }

  return <form action="#" method="get" accept-charset="utf-8">

    <p class={this.props.errors[this.props.errorsScope + '.' + 'firstname' ] ? "error" : ""}>
      <input type="text" onChange={this.onAddressChange} data-name="firstname" value={this.props.address.firstname} />
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='firstname' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'lastname' ] ? "error" : ""}>
      <input type="text" onChange={this.onAddressChange} data-name="lastname" value={this.props.address.lastname} />
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='lastname' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'address1' ] ? "error" : ""}>
      <input type="text" onChange={this.onAddressChange} data-name="address1" value={this.props.address.address1} />
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='address1' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'city' ] ? "error" : ""}>
      <input type="text" onChange={this.onAddressChange} data-name="city" value={this.props.address.city} />
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='city' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'zipcode' ] ? "error" : ""}>
      <input type="text" onChange={this.onAddressChange} data-name="zipcode" value={this.props.address.zipcode} />
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='zipcode' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'phone' ] ? "error" : ""}>
      <input type="text" onChange={this.onAddressChange} data-name="phone" value={this.props.address.phone} />
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='phone' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'country' ] ? "error" : ""}>
      <select onChange={this.onCountryChange}>
        <option>Select country</option>
        {CountriesOptions}
      </select>
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='country' />
    </p>

    <p class={this.props.errors[this.props.errorsScope + '.' + 'state' ] ? "error" : ""}>
      {StatesInput}
      <InlineError errors={this.props.errors} errorsScope={this.props.errorsScope} field='state' />
    </p>

  </form>;
}