/** @jsx React.DOM */
ShopApp.Templates.BuyButton = function() {
  var that = this;
  var BuyButtonVariant = ShopApp.Views.BuyButtonVariant;

  var product = this.props.product;
  var availableVariantList = this._cart.getAvailableVariants(product);
  var cartItemList = this._cart.findByProduct(product);


  // this part is a standard button for small product views
  if (!this.props.forProductPreview)
    return <div class="cart view">
      { cartItemList.length == 0 ?
        <button type="button" class="btn btn-large">
          <span onClick={this._addCartItem}>Add To Cart</span>
        </button>
      : '' }

      { cartItemList.length == 0 ?
        <p>
          { product.getNamedVariants().map(function (v) {
            return v.getVariantName();
          }).join(', ')}
        </p>
      : '' }

      { cartItemList.map(function (cartItem) {
        return <BuyButtonVariant cartItem={cartItem} />;
      })}

      { (cartItemList.length > 0 && availableVariantList.length > 0) ?
        <p style={{display: 'block'}} onClick={this._addCartItem}>
          + Add another variant
        </p>
      : '' }
    </div>;

  // this part is a template for large product preview
  else
    return <div class="buy clearfix">
      { cartItemList.length == 0 ?
        <h1 class="price">{m( product.get('price') )}</h1>
      : '' }

      { cartItemList.length == 0 ?
        <h5>
            { product.getNamedVariants().map(function (v) {
              return v.getVariantName();
            }).join(', ')}
        </h5>
      : '' }

      <div class="cart">

        { cartItemList.length == 0 ?
          <button type="button" class="btn">
            <span onClick={this._addCartItem}>Add To Cart</span>
          </button>
        : '' }

        { cartItemList.map(function (cartItem) {
          return <BuyButtonVariant cartItem={cartItem} />;
        })}

        { (cartItemList.length > 0 && availableVariantList.length > 0) ?
          <p style={{display: 'block'}} onClick={this._addCartItem}>
            + Add another variant
          </p>
        : '' }
      </div>
    </div>;
};
