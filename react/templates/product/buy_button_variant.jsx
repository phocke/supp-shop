/** @jsx React.DOM */
ShopApp.Templates.BuyButtonVariant = function() {
  var that = this;
  var availableVariants = this.props.cartItem.getAvailableVariants();
  var postfixChoosingVariant = this.state.choosingVariant ? ' view' : '';
  var product = this.props.cartItem.get('product')
  var chosenVariant = this.props.cartItem.get('variant');

  return <div class="cart-choose" onMouseLeave={this._onMouseLeave}>
    <div class={ "variant default clearfix" + postfixChoosingVariant }>
      <div class="variants">
        <i class="icon-caret-down icon-white"></i>

        <span onClick={this._toggleVariantMenu}>
          { chosenVariant.getVariantName() }
        </span>

        <ul class={ '' + postfixChoosingVariant }>
          { availableVariants.map(function (v) {
            var chooseThisVariant = function () { that._chooseVariant(v); };
            return <li onClick={chooseThisVariant}>
              <span> {v.getVariantName()} </span>
            </li>;
          }) }
        </ul>
      </div>

      <div class="qnty">
        <input type="text" value={this.state.quantityInputValue}
                            onChange={this._quantityInputChanged}/>
        <span>
          <i onClick={this._quantityInc} class="icon-caret-up"></i>
          <i onClick={this._quantityDec} class="icon-caret-down"></i>
        </span>
      </div>
      <p class="price">
        ${ this.props.cartItem.getPrice() }
      </p>
      <div class="remove" onClick={this._removeCartItem}>
        <i class="icon-remove"></i>
      </div>
    </div>
  </div>;
};
