/** @jsx React.DOM */
ShopApp.Templates.Products = function() {
  var Enumerate = ShopApp.Views.Enumerate;
  var ProductPreview = ShopApp.Views.ProductPreview;
  var Checkout = ShopApp.Views.Checkout;

  return <div>
    <div class="container" id="items">
      <div class="row" id="tiles">
        <Enumerate collection={this._presentedCollection}
          modelView={ShopApp.Views.Product}
          onDidUpdate={this._runWookmark}>
            <div class="span12 text-center enumerate-empty">
              <h3>Sorry, couldn't find any products.</h3>
            </div>
        </Enumerate>
      </div>

      <ProductPreview products={this._cachedCollection} />

      <Checkout />
    </div>
  </div>;
};
