/** @jsx React.DOM */
ShopApp.Templates.Product = function() {
  var BuyButton = ShopApp.Views.BuyButton;

  var productUrl = "#!/product/" + this.props.model.get('id');

  var taxonIcons = {
    'Brand': 'icon-tag',
    'Categories': 'icon-folder-close',
    'Form': 'icon-tint'
  };

  return <div ref="root"
              class={'span4 item' + (this.state.menuOpened ? ' menu' : '')}
              onMouseEnter={this._onMouseEnter}
              onMouseLeave={this._onMouseLeave}>

    <div class="menu">
      <ul class="nav">
        { this.state.taxons.map(function (taxon) {
          var taxonomyName = taxon.collection.taxonomy.get('name');
          return <li>
            <a href="#">Browse this {taxonomyName.toLowerCase()}</a>
            <button class="btn btn-green">
              <i class={ (taxonIcons[taxonomyName]) + " icon-white"}></i>
              { taxon.get('name') }
            </button>
          </li>;
        }) }
      </ul>
    </div>

    <div class="wrapper">
      <div class="item-buttons clearfix">
        <div class="options">
          <button class="btn btn-green menu-toggle" onClick={this._cardToggle}>
            <i class="icon-th-list"></i>
          </button>
          <button class="btn btn-green full-toggle" onClick={this._largePreview}>
            <i class="icon-resize-full"></i>
          </button>
        </div>
        <BuyButton product={this.props.model} />
      </div>

      <img src={this.props.model.getImages()[0].large_url}
            alt="Placeholder Image" class="full-toggle" />

      <div class="caption">

        <h3>
          <a class="full-toggle">
            <span class="price">{m(this.props.model.get('price'))}</span>
            {this.props.model.get('name')}
          </a>
        </h3>

        <div class="properties">
          <p>
            { this.state.taxons.map(function (taxon) {
              var taxonomyName = taxon.collection.taxonomy.get('name');
              return <a href="#" data-property="form">
                <i class={ (taxonIcons[taxonomyName]) + " icon-white"}></i>
                <span>{ taxon.get('name') }</span>
              </a>;
            }) }
          </p>
        </div>

      </div>
    </div>
  </div>;
};
