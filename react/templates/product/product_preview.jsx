/** @jsx React.DOM */
ShopApp.Templates.ProductPreview = function () {
  if(!this.state || !this.state.product)
    return <div class="item-full">
      <div class="bg full-toggle"></div>
      <div class="box"></div>
    </div>;

  var BuyButton = ShopApp.Views.BuyButton;

  var taxonIcons = {
    'Brand': 'icon-tag',
    'Categories': 'icon-folder-close',
    'Form': 'icon-tint'
  };

  return <div class={'item-full normal' + (this.state.active ? ' view' : '')} ref="root">

      <div class="bg full-toggle">
      </div>

      <div class="box">

        <div class="image">
          <i class="icon-remove circle" onClick={this._close} title="Press Esc to close"></i>
          <img src={this.state.product.getImages()[0].large_url} alt="" />
        </div>

        <div class="main">

          <div class="parameters">
            <h1>{this.state.product.get('name')}</h1>
            <p>
              { this.state.taxons.map(function (taxon) {
                var taxonomyName = taxon.collection.taxonomy.get('name');
                return <span>
                  <i class={ (taxonIcons[taxonomyName]) + " icon-white"}></i>
                  { taxon.get('name') }
                </span>
              }) }
            </p>
          </div>

          <div class="desc">
            <p>{this.state.product.get('description')}</p>
          </div>
          <BuyButton product={this.state.product} forProductPreview={true} />
        </div>
      </div>
  </div>;
};
