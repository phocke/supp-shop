/** @jsx React.DOM */
ShopApp.Templates.Cart = function() {
  var that = this;

  var Enumerate = ShopApp.Views.Enumerate;
  var CartItem = ShopApp.Views.CartItem;

  var LockButton = function () {
    if (that.state.locked)
      return <span class="pull-right" id="lock">
        <a href="#" onClick={that._unlock}>
          <i class="icon-caret-up circle"></i>
        </a>
      </span>;
    else
      return <span class="pull-right" id="lock">
        <a href="#" onClick={that._lock}>
          <i class="icon-caret-up circle"></i>
        </a>
      </span>;
  };

  var OpenButton = function () {
    return <span class="pull-right" id="lock">
      <i class="icon-caret-up circle"></i>
    </span>;
  };

  var CheckoutButton = function () {
    if (that.state.productsNumber === 0)
      return <button class="btn toggle-checkout disabled">
          <span class="hidden-phone">Checkout</span>
          <i class="icon-shopping-cart visible-phone"></i>
        </button>
    else
      return <button class="btn toggle-checkout" onClick={that._requestCheckout}>
          <span class="hidden-phone">Checkout</span>
          <i class="icon-shopping-cart visible-phone"></i>
        </button>
  }

  return <div
  class="navbar navbar-fixed-bottom" ref="root"
  onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave}>

    <div class="navbar-inner">
      <div class="container bar">
        <i class="icon-shopping-cart"></i>
        <a class="brand" href="#">Your Shopping Cart:</a>
        <span class="cart-count">
          {this.state.productsNumber > 0 ? this.state.productsNumber : 'No'}
          {this.state.productsNumber == 1 ? ' item' : ' items'},

          {m(this.state.summedPrice)}
        </span>

        {this.state.opened ? <LockButton /> : <OpenButton />}
      </div>
      <div class="container cart">
        <div id="items-cart">
          <div class="scrolls" ref="scrolls">
            <div class="inner" style={{width: this._cart.length * 172 + 'px'}}>
              { this._cart.map(function (cartItem) {
                return <CartItem key={cartItem.cid} model={cartItem} />;
              }) }
            </div>
          </div>
        </div>

        <div id="summary" class="summary pull-right">
          <div class="subtotal">
            {m(this.state.summedPrice)}
          </div>
          <CheckoutButton />
          <i class="icon-caret-left circle cart-toggle"></i>
        </div>
      </div>
    </div>
  </div>;
};
