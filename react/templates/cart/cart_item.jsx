/** @jsx React.DOM */
ShopApp.Templates.CartItem = function() {
  return <div class="item" ref="root"
  onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave}>

    <div class="left">
      <img src={this.props.model.get("product").getImages()[0].small_url} height="100" />

      <div class="hover" ref="hover">
        <div class="variants">
          <span>
            { this.props.model.get('variant').getVariantName() }
          </span>
        </div>
        <i title="Full View" class="icon-resize-full circle" onClick={this._preview}></i>
      </div>
      <i title="Remove" class="icon-remove" onClick={this._remove}></i>
    </div>

    <div class="right" ref="right">
      <div class="quantity">
        <input type="text" value={this.state.quantityInputValue} />
        <span>
          <i class="icon-caret-up" onClick={this._quantityIncrease}></i>
          <i class="icon-caret-down" onClick={this._quantityDecrease}></i>
        </span>
      </div>

      <div class="rate">
        {m(this.props.model.getPrice())}
      </div>
    </div>
  </div>;
};
