/** @jsx React.DOM */
ShopApp.Templates.Main = function() {
  var BackButton = ShopApp.Views.BackButton;
  var ProductDetails = ShopApp.Views.ProductDetails;
  var Products = ShopApp.Views.Products;
  var Filters = ShopApp.Views.Filters;
  var Cart = ShopApp.Views.Cart;

  return <div>
    <div id="main2">
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <a class="brand" href="#">Duplays</a>

            <ul class="nav pull-right">
              <li><a href="#">Link 1</a></li>
              <li><a href="#about">Link 2</a></li>
            </ul>

            <div class="btn-down" onClick={this._headSlideDown}>
              <span><i class="icon-caret-down circle"></i></span>
            </div>

          </div>
        </div>
      </div>

      <Cart />

      <div id="header-top" class="text-center">
        <div class="bg">

        </div>
        <div class="container">
          <div class="hero-unit">
            <h1>Hello, world!</h1>
            <p>
              This is a template for a simple marketing or informational website.
              It includes a large callout called the hero unit and three supporting pieces of content.
              Use it as a starting point to create something more unique.
            </p>
            <p>
              <button class="btn btn-large btn-up" id="header-slide-up" onClick={this._headSlideUp}>
                <span>Start Shopping</span>
                <i class="icon-caret-right circle"></i>
              </button>
            </p>
          </div>
        </div>
      </div>

      <Filters />

      <Products />

      <div class="container">
        <hr />

        <footer>
          <p>© Company 2013</p>
        </footer>
      </div>
    </div>
  </div>;
};
