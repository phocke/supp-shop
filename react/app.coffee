@ShopApp = ShopApp = {}
@ShopApp.Views = {}
@ShopApp.Templates = {}
@ShopApp.Models = {}

class Router extends Backbone.Router
  routes:
    "": "routeToBang"
    "!/(:nameContains)": "products"

  urls: new Backbone.Collection

  routeToBang: () ->
    setTimeout () =>
      @navigate '!/', trigger: true
    , 0

  products: (nameContainsKeyword) ->
    @trigger 'changeView',
      viewName: 'Products'
      nameContainsKeyword: nameContainsKeyword

$ () ->
  router = ShopApp.router = new Router
  React.renderComponent ShopApp.Views.Main({ router }),
    $('#supp-shop-container')[0]

  Backbone.history.start()
