////////////////////// UI //////////////////////

$.fn._headSlideUp = function() {
    $('#header-top').slideUp(400);
    $('#header-slide-down').show();
    $('#items').css('padding', '30px 0 0 0')
};

$.fn._headSlideDown = function() {
    $('#header-top').slideDown(400);
    $('#header-slide-down').hide();
    $('#items').css('padding', '0')
};

$.fn._iconWhite = function () {
  $(this).hover(function () {
    $(this).children('i').addClass('icon-white');
  },
  function () {
    $(this).children('i').removeClass('icon-white');
  });
}

$.fn._imagesLoaded = function( callback ){
    var elems = this.find( 'img' ),
        elems_src = [],
        self = this,
        len = elems.length;
    if ( !elems.length ) {
        callback.call( this );
        return this;
    }
    elems.one('load error', function() {
        if ( --len === 0 ) {
          len = elems.length;
          elems.one( 'load error', function() {
            if ( --len === 0 ) {
              callback.call( self );
            }
          }).each(function() {
            this.src = elems_src.shift();
          });
        }
    }).each(function() {
        elems_src.push( this.src );
        this.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
    });
    return this;
};

$.fn._filtersChoose = function () {
  $(this).click(function () {
    $(this).toggleClass('chosen');
    $(this).find('i').toggle();
  });
  $(this).hover(
    function () {
      $(this).children('img').attr('src', 'assets/img/man.png');
    }, function () {
      if (!$(this).hasClass('chosen')) { $(this).children('img').attr('src', 'assets/img/man-grey.png'); }
    }
  );
}

$.fn._advancedFilters = function () {
  $(this).fadeToggle(200);
  if ($(this).siblings('form').children('input').width() === 783) {
    $(this).siblings('form').children('input').animate({width: 340},600);
    $(this).siblings('.filter, .advanced').delay(800).fadeIn(200);
  } else {
    $(this).siblings('.filter').fadeOut(200);
    $(this).siblings('form').children('input').delay(200).animate({width: 783},600);
    $(this).siblings('.advanced').delay(800).fadeIn(200);
  }
}

$.fn._fullDisplay = function(item) {
    $(this).click(function () {
        $('.navbar-fixed-bottom').removeClass('opened');
        $(item).toggle('clip',400);
        if ($('.item-full').css('display') === 'block') {
          $('.item-full').find('.box')
            .css('width', 450+$(item).find('.box .right').width());
        };
        if (item === '#checkout') {
          $('.navbar-fixed-bottom').toggle();
          $('.navbar-fixed-bottom').css('bottom','-140px');
        }
    });
};

$.fn._descScroll = function () {
  $(this).mouseenter(function () {
    if ($('.item-full .desc p').height() > 230) { $(this).animate({top: 230-$(this).height()},5000); }
  }); 
  $(this).mouseleave(function () {
    $(this)
      .stop()
      .animate({top: 0},400);
  });  
};

$.fn._cardToggle = function() {
      if (!$(this).hasClass('item')) {
        $(this).parents('.item').toggleClass('flip');
      } else {
        $(this).removeClass('flip');
      };
};

$.fn._textSize = function () {
    if ($(this).height() != 40) {
      $(this).css('font-size',parseFloat($(this).css('font-size'))-1);
      $(this)._textSize();
    } else { return; }
}

$.fn._itemHov = function () {
    $(this).hover(function () {
      $(this).find('.caption').animate({bottom: 0}, 200);
      $(this).find('.front').children('img').animate({bottom: 20}, 200);
    }, function () {
      $(this).find('.caption').animate({bottom: -50}, 200);
      $(this).find('.front').children('img').animate({bottom: 0}, 200);
    });
}

$.fn._addCart = function () {
  $(this).click(function () {
    $(this)
      .parents('.item')
        .clone()
        .prependTo('#tiles')
        .css('z-index',100)
        .delay(200)
        .animate({top: 1000, opacity: 0, width: 0, height: 0}, 800);
    $(this)
      .parents('.item-full')
        .clone()
        .prependTo('#items')
        .css('z-index',100)
        .css('background','transparent')
        .delay(200).animate({top: 1000, opacity: 0, width: 0, height: 0}, 800); 
    $(this)
      .hide()
      .siblings('.cart-choose').show();
  });

  $(this).siblings('.cart-choose').children('.variants, .qnty').hover(
    function () {
      var desc;
      if ($(this).hasClass('variants')) {
        desc = "Choose<br>variant:";
      } else {
        desc = "Choose<br>quantity:";
      }
      $(this).siblings('.desc').html(desc);
    },
    function () {
      $(this).siblings('.desc').html("");
    }
  );
}

$.fn._toggleCart = function () {
      $('.navbar-fixed-bottom').toggleClass('opened');
      if (!$('.navbar-fixed-bottom').hasClass('fixed-opened')) {
        if ($('.navbar-fixed-bottom').hasClass('opened')) {
          $('.navbar-fixed-bottom').animate({bottom: 0},200);
        } else {
          $('.navbar-fixed-bottom').animate({bottom: -140},200);
        }
        $('#lock i')
          .toggleClass('icon-white')
          .toggleClass('icon-chevron-up')
          .toggleClass('icon-lock'); 
      }
}

$.fn._cartDisp = function () {
    $(this).find('.bar').mouseenter(function () {
      if (!$('.navbar-fixed-bottom').hasClass('opened')) { $(this)._toggleCart(); }
    });

    $(this).mouseleave(function () {
      $(this)._toggleCart();
    });

    $('#lock').click(function () {
      $(this).parents('.navbar-fixed-bottom').toggleClass('fixed-opened');      
    });
}

$.fn._itemHover = function () {

    $(this).hover(function () {
      if ($(this).find('img').width() <= 68) { $(this).find('.left span').hide(); }
      $(this).find('.hover').fadeIn(200);
      $(this).find('.right').animate({width: 200},400);
      $(this).find('.quantity, .rate').animate({width: 198},400);
      $(this).find('.price, .total span').show();
      $(this).find('.quantity').toggleClass('open');
      $(this).find('.total').css('width',99);
    },
    function () {
      $(this).find('.hover').fadeOut(200);
      $(this).find('.right').animate({width: 50},400);
      $(this).find('.quantity, .rate').animate({width: 48},400);
      $(this).find('.price, .total span').hide();
      $(this).find('.quantity').toggleClass('open');
      $(this).find('.total').css('width',48);
    }
    );
}

$.fn._removeItem = function () {
  $(this).parents('.item').fadeOut(200);
}

$.fn._checkoutPath = function () {
  $(this).find('section .step-content').hide();
  $(this).find('section.active .step-content').show();
  $('#checkout .right, #checkout .next').click(function () {
    $('section.active')
      .children('.step-content').slideUp(400);
    $('section.active')
      .removeClass('active')
      .next().addClass('active');
    $('section.next')
      .children('.step-content').slideDown(400);
    $('section.next')
      .removeClass('next')
      .next().addClass('next');
  })
}
