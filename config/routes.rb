SuppShop::Application.routes.draw do
  root :to => "home#index"
  mount Spree::Core::Engine, :at => '/'
end