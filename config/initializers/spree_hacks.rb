Spree::Core::ControllerHelpers::Order.class_eval do

  def current_order_with_token(create_order_if_necessary = false)
    if params[:order_token]
      token = Spree::TokenizedPermission.find_by_token(params[:order_token])

      if token && token.permissable.is_a?(Spree::Order)
        @current_order = token.permissable
      end

      return @current_order
    else
      return current_order_without_token
    end
  end

  alias_method_chain :current_order, :token

end