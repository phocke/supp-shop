# Be sure to restart your server when you modify this file.

SuppShop::Application.config.session_store :cookie_store, key: '_supp_shop_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# SuppShop::Application.config.session_store :active_record_store
